require("@nomiclabs/hardhat-waffle");

task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

const SEPOLIA_PRIVATE_KEY =
  "2669ce15f01dba9b4251411f4f76ed2b30ead2f0b6fed9367b1332ad6669ff9d";

module.exports = {
  solidity: "0.8.17",

  networks: {
    SEPOLIA: {
      url: "https://eth-sepolia.g.alchemy.com/v2/6YduJF6C0l_5pAHkxg2F0Lwb4tEcqocA",

      accounts: [`${SEPOLIA_PRIVATE_KEY}`],
    },
  },
};
