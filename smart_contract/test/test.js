const { expect } = require("chai");

describe("Identeefi", function () {
  let Identeefi;
  let identeefi;
  let owner;
  const serialNumber = "001";
  const name = "Product 1";
  const brand = "Brand 1";
  const description = "Description 1";
  const image = "Image URL 1";
  const actor = "Actor 1";
  const location = "Location 1";
  const timestamp = "Timestamp 1";

  beforeEach(async function () {
    Identeefi = await ethers.getContractFactory("Identeefi");
    identeefi = await Identeefi.deploy();
    await identeefi.deployed();
    [owner] = await ethers.getSigners();
  });

  it("Should register a product and retrieve its details", async function () {
    await identeefi.registerProduct(
      name,
      brand,
      serialNumber,
      description,
      image,
      actor,
      location,
      timestamp
    );
    const product = await identeefi.getProduct(serialNumber);

    expect(product[0]).to.equal(serialNumber);
    expect(product[1]).to.equal(name);
    expect(product[2]).to.equal(brand);
    expect(product[3]).to.equal(description);
    expect(product[4]).to.equal(image);
    expect(product[5].length).to.equal(1); // History size should be 1
    expect(product[5][0].id).to.equal(1); // History ID should start from 1
    expect(product[5][0].actor).to.equal(actor);
    expect(product[5][0].location).to.equal(location);
    expect(product[5][0].timestamp).to.equal(timestamp);
    expect(product[5][0].isSold).to.equal(false);
  });

  it("Should add product history", async function () {
    await identeefi.registerProduct(
      name,
      brand,
      serialNumber,
      description,
      image,
      actor,
      location,
      timestamp
    );
    const newActor = "Actor 2";
    const newLocation = "Location 2";
    const newTimestamp = "Timestamp 2";

    await identeefi.addProductHistory(
      serialNumber,
      newActor,
      newLocation,
      newTimestamp,
      true
    );
    const product = await identeefi.getProduct(serialNumber);

    expect(product[5].length).to.equal(2); // History size should be 2 now
    expect(product[5][1].id).to.equal(2); // New history entry should have ID 2
    expect(product[5][1].actor).to.equal(newActor);
    expect(product[5][1].location).to.equal(newLocation);
    expect(product[5][1].timestamp).to.equal(newTimestamp);
    expect(product[5][1].isSold).to.equal(true);
  });
});
